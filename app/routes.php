<?php
/*
*   Routes for my crappy drawings.
*   Southern Fried Pixels FTW!
*
*/
// Default Route
Route::get('/', function()
{
    $items = Item::all();
    return View::make('items')->with('items', $items);
});
// Display all Items 
Route::get('/items', function()
{
    $items = Item::all();
    return View::make('items')->with('items', $items);
});
// route new views.
Route::get('/api/items', function()
{
    $items =Item::all();
    return Response::JSON($items)->setCallback(Input::get('callback'));
});


// Route to create new item.
Route::get('/items/create', 'ItemsController@create');
// Route to get a single item.
Route::get('/items/show/{id}', 'ItemsController@show');
// Rotue to Controller to save upvote into db.
Route::post('/items/store', function(){
    $vote = new Upvote;
    $vote->item_id = Input::get('item_id');
    $vote->save();
    return Redirect::to('items/show/'.$vote->item_id);
});


// -------------- Admin Section --------------
// Route to login
Route::get('login', 'AuthController@login');
// get data from login form.
Route::post('login', 'AuthController@processlogin');
// Route to logout
Route::get('logout', function(){
    Auth::logout();
    return Redirect::to('login');
});
// Route to admin dashboard
Route::get('dashboard', array('before' => 'auth', function()
{
    $items =Item::all();
    return View::make('dash')->with('items', $items);
    
}));
// #TODO Route to admin add items.
Route::get('dashboard/create', 'AdminController@create');
Route::post('dashboard/store', 'AdminController@store');
// Route to admin update items.
Route::get('dashboard/edit/{id}', 'AdminController@edit');
Route::post('dashboard/edit/{id}', 'AdminController@update');
// Route to admin delete items.
Route::post('dashboard/destroy/{id}', 'AdminController@destroy');
// TODO Route to add user for admin
Route::get('dashboard/adduser', 'AdminController@adduser');
// TODO Route to get add user data
Route::post('dashboard/adduser','AdminController@createuser' );

//  #TODO Route to add to cart

// #TODO Route to remove item from cart

// #TODO Route to process form to Merchant