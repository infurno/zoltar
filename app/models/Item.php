<?php

class Item extends Eloquent {
    protected $table ='items';
    
    public function likes()
    {
        return $this->hasMany('item_id');
        
    }
    public function upvotes() 
    {
        return $this->hasMany('item_id');
    }
    protected $fillable = array('name', 'desc', 'image', 'amount');
}