<?php
class ItemsController extends \BaseController {
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
    /**
     * The layout for items.
     */
    protected $layout = 'layouts.master';
	
    public function index()
	{
		
        $this->layout->content = View::make('items');
	}
	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $data = Item::find($id);
        
        return View::make('item')->with('item', $data);
	}
    /**
     * Like an item.
     */
   
}