<?php
class AdminController extends \BaseController {
    /**
     * Instantiate a new UserController instance.
     */
    public function __construct()
    {
        $this->beforeFilter('auth');
    }
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return View::make('admin.add');
	    //echo "This is the create form";
	}
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{  
        $rules = array(
              'name' => 'required',
              'desc' => 'required',
              'image' => 'required',
              'amount' => 'required',
            );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails())
        {
            $messages = $validator->errors();
            //var_dump($messages);
            return Redirect::to('dashboard/create')->with('messages','Insert failed')->withInput();
        } else
        {
            $file = Input::file('image');
            // Move uploaded file for the web.
            $destinationPath = 'img/';
            $filename = $file->getClientOriginalName();
            $uploadSuccess = Input::file('image')->move($destinationPath, $filename);
            $data = array(
                'name' => Input::get('name'),
                'desc' => Input::get('desc'),
                'image' => $filename,
                'amount' => Input::get('amount')
                );
            $items = new Item($data);
            $items->save();
           return Redirect::to('dashboard');
        }
	}
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
    public function edit($id)
	{
        $data = Item::find($id);   
        return View::make('admin.item')->with('item', $data);
	}
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{	      
        $data = Item::find($id);
        $data->id = Input::get('id');
        $data->name = Input::get('name');
        $data->desc = Input::get('desc');
        $data->save();
        return Redirect::to('dashboard');
	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{   
        $id = Input::get('id');
        $item = Item::find($id);
        $item->delete();   
        return Redirect::to('dashboard');
        
	}
    public function adduser()
    {
        return View::make('admin.adduser');
    }
    public function createuser()
    {
        $userdata = array(
            'username' => Input::get('username'),
            'email' => Input::get('email'),
            'password' => Hash::make(Input::get('password')) );
        $user = new User($userdata);
        $user->save();
        return Redirect::to('dashboard');
    }
}