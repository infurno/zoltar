<?php

class AuthController extends \BaseController {
   
    
    public function login()
    {
        
        return View::make('admin.login');
    }
    public function processlogin()
    {
        $input = Input::all();

        if(Auth::attempt($input)){
            //echo "your in";
            return Redirect::to('dashboard');
        } else {       
            return Redirect::to('login')->withInput()->with('message', 'Login failed');
        }
    }
    
}