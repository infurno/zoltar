@extends('layouts.master')

@section('content')
    
    <div class="col-lg-6">
             <h1>{{ $item->name }} </h1>
            <p>{{ $item->desc }}</p>
           <a href="#" class="thumbnail">
             <img src="{{ URL::to('img') }}/{{ $item->image }}" alt="{{ $item->name }}"/>
           </a>           
            {{ Form::open(array('url'=> '/items/store','ng-submit' => 'submit()', 'ng-controller' => 'Ctrl')) }} 
                {{ Form::hidden('item_id', $item->id )}}
                {{ Form::submit('Vote', array('class'=>'btn btn-primary')) }}
            {{ Form::close()  }}
            <span class="badge">{{ Upvote::where('item_id', '=', $item->id)->count(); }}</span> likes
        <div class="comments">
            <div class="fb-comments" data-href="http://example.com/comments" data-numposts="5" data-colorscheme="light"></div>
        </div>
    </div>
   
@stop