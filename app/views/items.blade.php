@extends('layouts.master')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar.</p>
@stop

@section('content')
    @foreach($items as $item)
       <div class="col-lg-4">
             <h1>{{ $item->name }} </h1>
            <p>{{ $item->desc }}</p>
           <a href="items/show/{{ $item->id }}" class="thumbnail">
             <img src="img/{{ $item->image }}" alt="{{ $item->name }}"/>
           </a>
            <!--
            {{ Form::open(array('url'=> '/items/store')) }} 
                {{ Form::hidden('item_id', $item->id )}}
                {{ Form::submit('Vote', array('class'=>'btn btn-primary')) }}
            {{ Form::close()  }}
            -->
            <span class="badge">{{ Upvote::where('item_id', '=', $item->id)->count(); }}</span> likes
       </div>      
    @endforeach   
    
    <div class="pagination">
      
    </div>
@stop
