<!doctype html>
<html lang="en">
    <head>
        <title>Auth</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Latest compiled and minified CSS -->
        {{ HTML::style('//netdna.bootstrapcdn.com/bootstrap/3.0.0-rc1/css/bootstrap.min.css')}}
        {{ HTML::style('css/style.css') }}
    </head>
    <body>
     
        <div class="container">
          
            <form method="POST" action="{{ URL::to('login') }}">
              <fieldset>
                <legend>Login</legend>
                <label>Email</label>
                <input type="text" id="email" name="email" placeholder="email">
                <label>Password</lable>
                <input type="password" id="password" name="password">
                <input class="btn btn-primary" type="submit" value="login">
              </fieldset>
            </form>
             @if(Session::has('message')) 
                <div id="myAlert" class="alert alert-danger fade " data-alert="alert">
                    <h4>{{ Session::get('message') }}</h4>
                </div>
            @endif
        </div>

 <!-- Latest compiled and minified JavaScript -->
    {{ HTML::script('//netdna.bootstrapcdn.com/bootstrap/3.0.0-rc1/js/bootstrap.min.js') }}
    {{ HTML::script('//codeorigin.jquery.com/jquery-2.0.3.min.js') }}
    {{ HTML::script('js/app.js') }}
    <script>
        function showAlert() {
            $("#myAlert").addClass("in");
        }
        
        window.setTimeout(function () {
            showAlert();
        }, 30);
    </script>
</body>
</html>