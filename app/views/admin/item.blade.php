@extends('layouts.admin')

@section('content')
    <div class="row"
    <div class="span6">
        <form method="POST" action="{{ URL::to('dashboard/edit/') }}/{{ $item->id }}">
            <fieldset>
            <label>Title</label>  <br />
            <input type="text" name="name" id="name" value="{{ $item->name }} " /><br/>
            
            <label>Description</label>  <br />
            <textarea cols="50" rows="5" id="desc" name="desc">{{ $item->desc }}</textarea><br />
           <a href="#" class="thumbnail">
             <img src="/zoltar/public/img/{{ $item->image }}" alt="{{ $item->name }}"/>
           </a>
           <p><input name="id" type="hidden" value="{{ $item->id }}"/></p>
           <p><input class="btn btn-primary" type="submit" value="Save"></p>
           </fieldset>
         </form>
        
       
        
    </div>
  </div>
   
@stop