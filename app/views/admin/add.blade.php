@extends('layouts.admin')

@section('sidebar')
    <div class="col-lg-4">
     <h1>Add a new Item</h1>
    <a href="{{ URL::to('dashboard/create') }}"><button class="btn btn-primary">Add</button></a>
    </div>
@stop

@section('content')
   
    {{ Form::open(array('url'=> 'dashboard/store', 'files' => true)) }} 
        {{ Form::label('name', 'Image Name :') }}<br />
        {{ Form::text('name') }} <br />
        {{ Form::label('desc', 'Description :') }}<br />
        {{ Form::textarea('desc') }} <br />
        {{ Form::label('image','Image to upload :') }}
        {{ Form::file('image') }}
        {{ Form::label('amount', 'Price of item :') }}
        {{ Form::text('amount') }} 
        
        {{ Form::submit('Add', array('class'=>'btn btn-primary')) }}
    {{ Form::close()  }}
    
@stop