<!doctype html>
<html lang="en">
    <head>
        <title>Add user</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0-rc1/css/bootstrap.min.css">
        <link rel="stylesheet" href="/zoltar/public/css/style.css" />
    </head>
    <body>
         <div class="container ">
            <div class="navbar navbar-inverse">
              <a class="navbar-brand" href="{{ URL::to('dashboard') }}">Zolta Admin</a>
              <ul class="nav navbar-nav">
                <li ><a href="{{ URL::to('dashboard') }}">Home</a></li>
                <li class="active"><a href="{{ URL::to('dashboard/adduser') }}">Adduser</a></li>
                <li ><a href="{{ URL::to('logout') }}">Logout</a></li>
              </ul>
            </div>
        </div>
        <div class="container">

        <form method="POST" action="{{ URL::to('dashboard/adduser') }}">
             <fieldset>
                <legend>Add user</legend>
                <label>Username</label>
            <p><input type="text" id="username" name="username" placeholder="username"></p>
                <label>Email</label>
            <p><input type="text" id="email" name="email" placeholder="email"></p>
                <label>Password</label>
            <p><input type="password" id="password" name="password"></p>
                <label>Comfirm Password</label>
            <p><input type="password" id="confirm_password" name="confirm_password"></p>
            <p><input class="btn  btn-primary" type="submit" value="Add User"></p>
            </fieldset>
        </form>
        
        
        
        </div>
     <footer class="container">
            <div class="col-lg-3">
                <h1>Footer Part 1 </h1>
                <p>kfkkdfs</p>
               
           </div>
           <div class="col-lg-3">
                 <h1>Things you can do</h1>
                <p>Do what you like!</p>
           </div>
           <div class="col-lg-3">
                 <h1>Footer Part 3 </h1>
                <p>asfdads asdf asdf sadf asdfasdf asfds</p>
           </div>
           <div class="col-lg-3">
                 <h1>Footer Part 4 </h1>
                <p>asfdads asdf asdf sadf asdfasdf asfds</p>
           </div>
        </footer>
       
    <!-- Latest compiled and minified JavaScript -->
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.0-rc1/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular.min.js"></script>
    <script src="https://raw.github.com/mgcrea/angular-strap/v0.7.5/dist/angular-strap.js"></script>
    <script src="/zoltar/public/js/app.js"></script>
 </body>
</html>    
               