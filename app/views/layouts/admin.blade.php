<!doctype html>
<html lang="en">
    <head>
        <title>Items</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Latest compiled and minified CSS -->
        {{ HTML::style('//netdna.bootstrapcdn.com/bootstrap/3.0.0-rc1/css/bootstrap.min.css')}}
        {{ HTML::style('css/style.css') }}
    </head>
    <body>
        <div class="container ">
            <div class="navbar navbar-inverse">
              <a class="navbar-brand" href="{{ URL::to('dashboard') }}">Zoltar Admin</a>
              <ul class="nav navbar-nav">
                <li class="active"><a href="{{ URL::to('dashboard') }}">Home</a></li>
                <li ><a href="{{ URL::to('dashboard/adduser') }}">Adduser</a></li>
                <li ><a href="{{ URL::to('logout') }}">Logout</a></li>
              </ul>
            </div>
        </div>
        <div class="container">
            @yield('sidebar')
        </div>
        <div class="container">
            @yield('content')
        </div>
        
        <footer class="container">
            <div class="col-lg-3">
                @yield('footer1')
               
           </div>
           <div class="col-lg-3">
                 
           </div>
           <div class="col-lg-3">
                 @yield('footer2')
           </div>
           <div class="col-lg-3">
                 <h1>Footer Part 4 </h1>
                <p>asfdads asdf asdf sadf asdfasdf asfds</p>
           </div>
        </footer>
    <!-- Latest compiled and minified JavaScript -->
    {{ HTML::script('//netdna.bootstrapcdn.com/bootstrap/3.0.0-rc1/js/bootstrap.min.js') }}
    {{ HTML::script('//codeorigin.jquery.com/jquery-2.0.3.min.js') }}
    {{ HTML::script('js/app.js') }}
</html>