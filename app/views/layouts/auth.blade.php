<!doctype html>
<html lang="en">
    <head>
        <title>Auth</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Latest compiled and minified CSS -->
        {{ HTML::style('//netdna.bootstrapcdn.com/bootstrap/3.0.0-rc1/css/bootstrap.min.css')}}
        {{ HTML::style('css/style.css') }}
    </head>
    <body>
       
        <div class="container">
            @yield('content')
        </div>
        
       
    <!-- Latest compiled and minified JavaScript -->
    {{ HTML::script('//netdna.bootstrapcdn.com/bootstrap/3.0.0-rc1/js/bootstrap.min.js') }}
    {{ HTML::script('//codeorigin.jquery.com/jquery-2.0.3.min.js') }}
    {{ HTML::script('js/app.js') }}
 </body>
</html>