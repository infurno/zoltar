@extends('layouts.admin')

@section('sidebar')

@stop

@section('content')
    <table class="table table-hover">
      <thead>
        <tr>
          <th>Name</th>
          <th>Description</th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
    @foreach($items as $item)
            <tr>
                <td>{{ $item->name }} </td>
                <td>{{ $item->desc }} </td>
                <td><a href="{{ URL::to('dashboard/edit/') }}/{{ $item->id }}" ><button class="btn">Edit </button></a></td>
                <td>
                    
                    {{ Form::open(array('url'=> 'dashboard/destroy/{$id}')) }} 
                    {{ Form::hidden('id', $item->id )}}
                    {{ Form::submit('Delete', array('class'=>'btn btn-danger')) }}
                    {{ Form::close()  }}
                </td>
    @endforeach
      </tbody>
    </table>
    
    
@stop

@section('footer1')
      <div class="span5">
     <h4>Add a new Item</h>
    <a href="{{ URL::to('dashboard/create') }}"><button class="btn btn-primary">Add</button></a>
    </div>
@stop

@section('footer2')
   
@stop