<?php

class UsersTableSeeder extends Seeder {
    public function run()
    {
        DB::table('users')->delete();
        
        User::create(array(
            'username' => 'hal',
            'email' => 'hal.borland@gmail.com',
            'password' => Hash::make('1234')
        ));
        User::create(array(
            'username' => 'test',
            'email' => 'test@example.com',
            'password' => Hash::make('1234')
        ));
    }
}
