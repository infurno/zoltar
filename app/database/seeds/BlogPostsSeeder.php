<?php
class BlogPostsSeeder extends Seeder {

    public function run()
    {
        DB::table('blog_posts')->delete();

        BlogPosts::create(array(
            'author' => 'Steven Klar',
            'text' => 'Some blog text'
        ));
    }

}