<?php

class ItemsTableSeeder extends Seeder {
    public function run()
    {
        DB::table('items')->delete();
        
        Item::create(array(
                'name' => 'Sunrise',
                'desc' => 'Sunrise run on Lake Harrison.',
                'image' => '1.jpg',
                'amount' => '3.50'
            ));
        Item::create(array(
            'name' => 'Path',
            'desc' => 'Path to camp site.',
            'image' => '2.jpg',
            'amount' => '3.50'
        ));
        Item::create(array(
                'name' => 'Blue Crane',
                'desc' => 'The blue crane watchs over me.',
                'image' => '3.jpg',
                'amount' => '3.50'
            ));
        Item::create(array(
            'name' => 'Flair',
            'desc' => '37 pieces of flair is the minimum.',
            'image' => '4.jpg',
            'amount' => '3.50'
        ));
        Item::create(array(
            'name' => 'Crawdad Days',
            'desc' => 'Crawdad Days 2013. ',
            'image' => '5.jpg',
            'amount' => '3.50'
        ));        
    }
}
