<?php

use Illuminate\Database\Migrations\Migration;

class CreateItemTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
	    Schema::create('items', function($table){
           $table->increments('id');
           $table->string('name');
           $table->string('desc');
           $table->string('image');
           $table->decimal('amount', 5,2);
           $table->timestamps();
	    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}