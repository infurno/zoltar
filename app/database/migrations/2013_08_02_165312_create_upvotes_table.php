<?php

use Illuminate\Database\Migrations\Migration;

class CreateUpvotesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		    Schema::create('upvotes', function($table) {
			$table->increments('id');
			$table->string('item_id', 128);
			$table->timestamps();
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('upvotes');    
	}

}