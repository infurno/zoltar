var app = angular.module('myApp', []);

app.controller('ItemsCtrl', function ($scope, $http){
	$http.get('/api/items/').success(function(data) {
		$scope.items = data;
	});
});