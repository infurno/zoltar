var app = angular.module('myApp', []);
app.factory('myApp', function($http) {
	return {
		getItems: function() {
			// return $http.get('/api/items').then(function(result) {
			// 	//console.log(result);
			// 	return result.data;
			// });
			var data = $http.get('/api/items');
			return data;
		}
	}
});

app.controller('ItemsCtrl', function($scope, myApp) {
	myApp.getItems().then(function(data){
		$scope.items = data;
		console.log(data);
	});
});